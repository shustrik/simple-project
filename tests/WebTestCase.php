<?php
namespace Tests;

use Doctrine\DBAL\Connection;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\HttpFoundation\Response;

class WebTestCase extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    public function setUp()
    {
        parent::setUp();
        $kernel = static::bootKernel();
        $connection = $kernel->getContainer()->get('doctrine.orm.entity_manager')->getConnection();
        $this->clearDb($connection);
    }

    protected function postJson(Client $client, string $uri, array $content): Response
    {
        return $this->jsonRequest('POST', $client, $uri, $content);
    }

    protected function putJson(Client $client, string $uri, array $content): Response
    {
        return $this->jsonRequest('PUT', $client, $uri, $content);
    }

    protected function getJson(Client $client, string $uri): Response
    {
        return $this->jsonRequest('GET', $client, $uri);
    }

    protected function jsonRequest(string $method, Client $client, string $uri, array $content = []): Response
    {
        $client->request(
            $method,
            $this->applyVersion($uri),
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',

            ],
            json_encode($content)
        );

        return $client->getResponse();
    }

    protected function applyVersion(string $uri)
    {
        return $uri . '?version=v1';
    }

    protected static function getKernelClass()
    {
        return '\AppKernel';
    }

    private function clearDb(Connection $connection)
    {
        $connection->beginTransaction();
        try {
            $connection->query('DELETE FROM campaigns');
            $connection->query('DELETE FROM campaign_types');
            $connection->query('DELETE FROM users');
            $connection->query('ALTER SEQUENCE users_id_seq RESTART');
            $connection->query('ALTER SEQUENCE campaigns_id_seq RESTART');
            $connection->query('ALTER SEQUENCE campaign_types_id_seq RESTART');
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            throw $e;
        }
    }
}