<?php

namespace Tests\CampaignBundle\Controller;

use Tests\WebTestCase;

class CampaignControllerTest extends WebTestCase
{
    private $authenticatedClient;

    public function setUp()
    {
        parent::setUp();
        $this->initAuthenticatedClient();
    }

    public function testCreate()
    {
        $createCampaign = $this->createCampaignType();
        $this->assertEquals(200, $createCampaign->getStatusCode());

        $create = $this->createCampaign();
        $this->assertEquals(200, $create->getStatusCode());
    }

    public function testShow()
    {
        $this->createCampaignType();
        $this->createCampaign();

        $show = $this->getJson($this->authenticatedClient, $this->campaignUri());
        $this->assertEquals(200, $show->getStatusCode());
        $this->assertEquals(
            '{"name":"Super campaign","user":{"username":"admin@campaign.com","email":"admin@campaign.com"},"campaignType":{"name":"Campaign Type","status":"enable"},"settings":{"priority":1}}',
            $show->getContent()
        );
    }

    public function testEdit()
    {
        $this->createCampaignType();
        $this->createCampaign();

        $edit = $this->putJson(
            $this->authenticatedClient,
            $this->campaignUri(),
            ['name' => 'Mega campaign', 'settings' => ['priority' => 2], 'campaignType' => 1]
        );
        $this->assertEquals(200, $edit->getStatusCode());

        $show = $this->getJson($this->authenticatedClient, $this->campaignUri());
        $this->assertEquals(
            '{"name":"Mega campaign","user":{"username":"admin@campaign.com","email":"admin@campaign.com"},"campaignType":{"name":"Campaign Type","status":"enable"},"settings":{"priority":2}}',
            $show->getContent()
        );
    }

    public function testDelete()
    {
        $this->createCampaignType();
        $this->createCampaign();

        $delete = $this->jsonRequest('DELETE', $this->authenticatedClient, $this->campaignUri());
        $this->assertEquals(200, $delete->getStatusCode());

        $show = $this->getJson($this->authenticatedClient, $this->campaignUri());
        $this->assertEquals(404, $show->getStatusCode());
    }

    private function createCampaign()
    {
        return $this->postJson(
            $this->authenticatedClient,
            '/api/campaign',
            ['name' => 'Super campaign', 'settings' => ['priority' => 1], 'campaignType' => 1]
        );
    }

    private function createCampaignType()
    {
        return $this->postJson($this->authenticatedClient, '/api/campaign-type', ['name' => 'Campaign Type']);
    }

    private function campaignUri()
    {
        return sprintf('/api/campaign/%d', 1);
    }

    private function initAuthenticatedClient()
    {
        $this->createTestUser('admin@campaign.com', 'password');
        $this->authenticatedClient = static::createClient(
            [],
            [
                'PHP_AUTH_USER' => 'admin@campaign.com',
                'PHP_AUTH_PW' => 'password'
            ]
        );
    }

    private function createTestUser($email, $password)
    {
        return $this->postJson(
            static::createClient(),
            '/api/user/registration',
            ['username' => $email, 'email' => $email, 'plainPassword' => ['first' => $password, 'second' => $password]]
        );
    }
}
