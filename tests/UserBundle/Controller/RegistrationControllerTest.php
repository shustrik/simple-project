<?php

namespace Tests\UserBundle\Controller;

use Tests\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testRegister()
    {
        $response = $this->postJson(
            static::createClient(),
            '/api/user/registration',
            ['username' => 'test23', 'email' => 'test23@test.my', 'plainPassword' => ['first' => 123, 'second' => 123]]
        );

        $this->assertEquals(200, $response->getStatusCode());
    }
}
