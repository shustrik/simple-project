<?php
declare(strict_types=1);

namespace CoreBundle\Controller;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;

trait ControllerTrait
{
    /**
     * @param Form $form
     *
     * @return array
     */
    private function invalidFormRequest(Form $form)
    {
        $errors = iterator_to_array($form->getErrors(true));
        $result = [];
        /** @var FormError $error */
        foreach ($errors as $error) {
            $payload = $error->getCause()->getConstraint()->payload;
            $currentError = [];
            $currentError['message'] = $error->getMessage();
            $currentError['field'] = $error->getOrigin()->getName();
            $currentError['code'] = $payload['code'] ?? 0;
            $result[] = $currentError;
        }

        return $result;
    }
}
