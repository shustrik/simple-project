<?php
declare(strict_types=1);

namespace CoreBundle\EventListener;

use CoreBundle\Exception\CheckSumException;
use CoreBundle\Exception\DoubleRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class ExceptionListener
 */
final class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $response = new JsonResponse();
        if ($exception instanceof CheckSumException) {
            $response->setData(['error' => '009', 'message' => $exception->getMessage()]);
            $response->setStatusCode($exception->getCode());
        } elseif ($exception instanceof DoubleRequestException) {
            $response->setData(['error' => '011', 'message' => $exception->getMessage()]);
            $response->setStatusCode($exception->getCode());
        } elseif ($exception instanceof \Exception) {
            $response->setData(['error' => 'Internal Server Error', 'message' => $exception->getMessage()]);
        }

        $event->setResponse($response);
    }
}
