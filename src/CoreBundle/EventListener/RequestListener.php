<?php
declare(strict_types=1);

namespace CoreBundle\EventListener;

use CoreBundle\Exception\CheckSumException;
use CoreBundle\Exception\DoubleRequestException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use UserBundle\Security\Token\TokenFinder;

final class RequestListener
{
    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * CacheBlacklistIP constructor.
     *
     * @param AdapterInterface $cache
     */
    public function __construct(
        AdapterInterface $cache
    ) {
        $this->cache = $cache;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if ($request->getMethod() == 'POST' || $request->getMethod() == 'PUT') {
//            $this->checkDoubleRequest($request);
//            $this->checkSum($request);
        }
    }

    private function checkDoubleRequest(Request $request)
    {
        $hash = sha1($request->getContent());
        $item = $this->cache->getItem($hash);
        if ($item->isHit()) {
            throw new DoubleRequestException('double request', 422);
        }

        $item->expiresAfter(10);
        $item->set($hash);
        $this->cache->save($item);
    }

    private function checkSum(Request $request)
    {
        $token = TokenFinder::find($request);
        $hash = hash('sha256', $token . $request->getContent());

        if ($request->headers->get('hash') !== $hash) {
            throw new CheckSumException('checkSum error', 422);
        }
    }
}
