<?php
declare(strict_types=1);

namespace CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CoreBundle
 */
final class CoreBundle extends Bundle
{
}
