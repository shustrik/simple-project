<?php
declare(strict_types=1);

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;

class AbstractForm extends AbstractType
{
    public function getBlockPrefix()
    {
        return '';
    }
}
