<?php
/**
 * wst-dev
 * User: shustrik
 * Date: 3/17/15
 * Time: 4:31 PM.
 */

namespace CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use CoreBundle\Doctrine\Types\JsonbArrayType;

/**
 * Class DoctrineConfigurationPass.
 */
class DoctrineConfigurationPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $this->configureCustomTypes($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    private function configureCustomTypes(ContainerBuilder $container)
    {
        $types = [];
        if ($container->hasParameter('doctrine.dbal.connection_factory.types')) {
            $types = $container->getParameter('doctrine.dbal.connection_factory.types');
        }
        $types[JsonbArrayType::NAME] = ['class' => 'CoreBundle\Doctrine\Types\JsonbArrayType', 'commented' => false];

        $container->setParameter('doctrine.dbal.connection_factory.types', $types);
        $container->getDefinition('doctrine.dbal.default_connection')->replaceArgument(
            3,
            ['JSONB' => JsonbArrayType::NAME]
        );
    }
}
