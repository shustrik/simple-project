<?php
declare(strict_types=1);

namespace UserBundle\Controller;

use CoreBundle\Controller\ControllerTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Form\RegistrationType;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/user/registration")
 *
 * Class RegistrationController
 * @Security("not is_granted('IS_AUTHENTICATED_FULLY')")
 */
final class RegistrationController extends FOSRestController
{
    use ControllerTrait;

    /**
     * @Route("", methods={"POST"})
     *
     * @param Request              $request
     * @param FormFactoryInterface $factory
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @Model(type=RegistrationType::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Register User",
     *     @SWG\Schema(
     *          type="string",
     *          example={"ok"}
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request"
     * )
     */
    public function registerAction(Request $request, FormFactoryInterface $factory)
    {
        $form = $factory->create(RegistrationType::class, new User());
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return $this->handleView($this->view(['Bad Request'], 400));
        }
        if (!$form->isValid()) {
            return $this->handleView($this->view($this->invalidFormRequest($form), 400));
        }
        $this->flush($form->getData());

        return $this->handleView($this->view(['ok'], 200));
    }

    /**
     * @param $object
     */
    private function flush(User $object)
    {
        $this->getDoctrine()->getManager()->persist($object);
        $this->getDoctrine()->getManager()->flush();
    }
}
