<?php
declare(strict_types=1);

namespace UserBundle\Entity;

use CoreBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser implements \JsonSerializable
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="CampaignBundle\Entity\Campaign", mappedBy="user")
     */
    protected $campaigns;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->enabled = true;
        $this->salt = base_convert(sha1(uniqid((string)mt_rand(), true)), 16, 36);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'username'         => $this->username,
            'email'         => $this->email
        ];
    }
}
