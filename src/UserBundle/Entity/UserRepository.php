<?php
declare(strict_types=1);

namespace UserBundle\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class UserRepository extends EntityRepository
{
    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->_em = $entityManager;
        $this->repository = $entityManager->getRepository(User::class);
    }

    public function findOneById(int $id)
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getOneOrNullResult();
    }

    public function add(User $user)
    {
        $this->_em->persist($user);
    }
}
