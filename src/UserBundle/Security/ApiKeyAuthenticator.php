<?php

namespace UserBundle\Security;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use UserBundle\Entity\User;
use UserBundle\Security\Token\ApiToken;
use UserBundle\Security\Token\TokenFinder;

/**
 * Class ApiKeyAuthenticator
 */
class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    /**
     * @var UserCheckerInterface
     */
    private $userChecker;
    /** @var AdapterInterface */
    private $adapter;

    /**
     * ApiKeyAuthenticator constructor.
     *
     * @param AdapterInterface     $adapter
     * @param UserCheckerInterface $userChecker
     */
    public function __construct(
        AdapterInterface $adapter,
        UserCheckerInterface $userChecker
    ) {
        $this->adapter = $adapter;
        $this->userChecker = $userChecker;
    }

    /**
     * @param TokenInterface        $token
     * @param UserProviderInterface $userProvider
     * @param                       $providerKey
     *
     * @return TokenInterface
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $apiKey = $token->getCredentials();
        $item = $this->adapter->getItem($apiKey);

        if (!$item->isHit()) {
            throw new AuthenticationException('Authentication failed. Token Expired');
        }
        $user = $userProvider->loadUserById(current(explode('.' ,base64_decode($apiKey))));

        return new ApiToken(
            $user,
            $apiKey,
            $providerKey,
            $user->getRoles()
        );
    }

    /**
     * @param TokenInterface $token
     * @param                $providerKey
     *
     * @return bool
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof ApiToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param Request $request
     * @param         $providerKey
     *
     * @return ApiToken
     */
    public function createToken(Request $request, $providerKey)
    {
        $token = TokenFinder::find($request);
        if (!$token) {
            return null;
        }

        return new ApiToken(
            'anon.',
            $token,
            $providerKey
        );
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(['error' => $exception->getMessageKey()], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
