<?php
declare(strict_types=1);

namespace UserBundle\Security\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class ApiToken extends AbstractToken
{
    /**
     * @var
     */
    private $token;
    /**
     * @var
     */
    private $providerKey;

    /**
     * ApiToken constructor.
     *
     * @param array $user
     * @param       $token
     * @param       $providerKey
     * @param       $attributes
     * @param array $roles
     */
    public function __construct($user, $token, $providerKey, $attributes = [], $roles = [])
    {
        parent::__construct($roles);
        $this->setAuthenticated(true);
        $this->setUser($user);
        $this->token = $token;
        $this->providerKey = $providerKey;
        $this->setAttributes($attributes);
    }

    /**
     * Returns the user credentials.
     *
     * @return mixed The user credentials
     */
    public function getCredentials()
    {
        return $this->token;
    }

    public function getProviderKey()
    {
        return $this->providerKey;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([$this->token, $this->providerKey, parent::serialize()]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list($this->token, $this->providerKey, $parentStr) = unserialize($serialized);
        parent::unserialize($parentStr);
    }
}