<?php
declare(strict_types=1);


namespace UserBundle\Security\Token;


use Symfony\Component\HttpFoundation\Request;

final class TokenFinder
{
    const QUERY = 'token';
    const HEADER = 'authorization';
    const SCHEMA = 'Bearer';

    /**
     * @param Request $request
     * @param string  $queryParameter
     * @param string  $header
     * @return array|mixed|string
     */
    public static function find(Request $request, $queryParameter = self::QUERY, $header = self::HEADER)
    {
        if ($request->query->has($queryParameter) && $token = $request->query->get($queryParameter, false)) {
            return $token;
        }
        if ($request->headers->has($header)) {
            $headerParts = explode(' ', $request->headers->get($header));
            if (count($headerParts) === 2 && $headerParts[0] === self::SCHEMA) {
                return $headerParts[1];
            }
        }

        return null;
    }
}