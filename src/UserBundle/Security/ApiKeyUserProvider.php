<?php
declare(strict_types=1);

namespace UserBundle\Security;

use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use UserBundle\Entity\User;
use UserBundle\Entity\UserRepository;

class ApiKeyUserProvider implements UserProviderInterface
{
    protected $repository;

    /**
     * UserProvider constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->repository->findOneBy(['username' => $username]);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $user;
    }

    public function loadUserById(int $id)
    {
        $user = $this->repository->findOneById($id);
        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Id "%s" does not exist.', $id));
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(
                sprintf('Expected an instance of WST\UserBundle\Entity\User, but got "%s".', get_class($user))
            );
        }

        try {
            $reloadedUser = $this->repository->findOneBy(['id' => $user->getId()]);
        } catch (NoResultException $e) {
            throw new UsernameNotFoundException(sprintf('User with ID "%d" could not be reloaded.', $user->getId()));
        }

        return $reloadedUser;
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}