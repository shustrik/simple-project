<?php

namespace UserBundle\Security\Handler;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class ApiAuthenticationHandler implements AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface
{
    /** @var AdapterInterface */
    private $adapter;

    /**
     * ApiKeyAuthenticator constructor.
     *
     * @param AdapterInterface $adapter
     */
    public function __construct(
        AdapterInterface $adapter
    ) {
        $this->adapter = $adapter;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $token = $this->createApiToken($token);

        return new JsonResponse(['token' => $token]);
    }

    private function createApiToken(TokenInterface $token)
    {
        $hash = base64_encode($token->getUser()->getId() . '.' . uniqid());
        $item = $this->adapter->getItem($hash);

        $item->expiresAfter(36000);
        $item->set($hash);
        $this->adapter->save($item);

        return $hash;
    }
}