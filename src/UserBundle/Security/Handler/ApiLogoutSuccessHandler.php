<?php

namespace UserBundle\Security\Handler;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use UserBundle\Security\Token\TokenFinder;

class ApiLogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    /** @var AdapterInterface */
    private $adapter;

    /**
     * ApiKeyAuthenticator constructor.
     *
     * @param AdapterInterface $adapter
     */
    public function __construct(
        AdapterInterface $adapter
    ) {
        $this->adapter = $adapter;
    }
    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @param Request $request
     *
     * @return Response never null
     */
    public function onLogoutSuccess(Request $request)
    {
        $this->removeToken(TokenFinder::find($request));
        return new JsonResponse(['status' => 'ok', 'message' => 'logout']);
    }

    private function removeToken($token)
    {
        $item = $this->adapter->getItem($token);
        if ($item->isHit()) {
            $this->adapter->deleteItem($token);
        }

        return true;
    }
}
