<?php
declare(strict_types=1);

namespace CampaignBundle\Entity\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
final class CampaignTypeStatus
{
    private const ENABLE = 'enable';
    private const DISABLE = 'disable';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     */
    private $status;

    /**
     * CampaignTypeStatus constructor.
     *
     * @param string $status
     */
    private function __construct(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return CampaignTypeStatus
     */
    public static function enable()
    {
        return new self(self::ENABLE);
    }

    /**
     * @return CampaignTypeStatus
     */
    public static function disable()
    {
        return new self(self::DISABLE);
    }

    /**
     * @return string
     */
    public function status()
    {
        return $this->status;
    }
}
