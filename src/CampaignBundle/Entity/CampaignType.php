<?php
declare(strict_types=1);

namespace CampaignBundle\Entity;

use CampaignBundle\Entity\ValueObject\CampaignTypeStatus;
use CoreBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="campaign_types")
 */
class CampaignType implements \JsonSerializable
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var CampaignTypeStatus
     * @ORM\Embedded(class="CampaignBundle\Entity\ValueObject\CampaignTypeStatus", columnPrefix=false)
     */
    protected $status;

    /**
     * CampaignType constructor.
     */
    public function __construct()
    {
        $this->status = CampaignTypeStatus::enable();
    }

    public function name()
    {
        return $this->name;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return ['name' => $this->name(), 'status' => $this->status->status()];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}
