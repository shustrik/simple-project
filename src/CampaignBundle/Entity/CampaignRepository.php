<?php
declare(strict_types=1);


namespace CampaignBundle\Entity;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class CampaignRepository extends EntityRepository
{
    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->_em = $entityManager;
        $this->repository = $entityManager->getRepository(Campaign::class);
    }

    public function add(Campaign $campaign)
    {
        $this->_em->persist($campaign);
    }

    public function remove(Campaign $campaign)
    {
        $this->_em->remove($campaign);
    }
}

