<?php
declare(strict_types=1);

namespace CampaignBundle\Entity;

use CoreBundle\Entity\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="campaigns")
 */
class Campaign implements \JsonSerializable
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="settings", type="json_array", options={"jsonb": true}, length=255, nullable=false)
     */
    protected $settings;

    /**
     * @var \UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="campaigns")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    protected $user;

    /**
     * @var \CampaignBundle\Entity\CampaignType
     *
     * @ORM\ManyToOne(targetEntity="CampaignBundle\Entity\CampaignType")
     * @ORM\JoinColumn(name="campaign_type_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    protected $campaignType;

    /**
     * Campaign constructor.
     *
     * @param User $user
     */
    public function __construct(
        User $user
    ) {
        $this->user = $user;
    }

    public function name()
    {
        return $this->name;
    }

    public function settings()
    {
        return $this->settings;
    }

    public function campaignType()
    {
        return $this->campaignType;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'name'         => $this->name,
            'user'         => $this->user,
            'campaignType' => $this->campaignType,
            'settings'     => $this->settings
        ];
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param CampaignType $campaignType
     */
    public function setCampaignType(CampaignType $campaignType)
    {
        $this->campaignType = $campaignType;
    }
}
