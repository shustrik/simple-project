<?php
declare(strict_types=1);

namespace CampaignBundle\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class CampaignTypeRepository extends EntityRepository
{
    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->_em = $entityManager;
        $this->repository = $entityManager->getRepository(CampaignType::class);
    }

    public function add(CampaignType $campaignType)
    {
        $this->_em->persist($campaignType);
    }
}
