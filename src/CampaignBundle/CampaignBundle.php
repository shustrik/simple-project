<?php
declare(strict_types=1);

namespace CampaignBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CampaignBundle
 */
final class CampaignBundle extends Bundle
{

}
