<?php
declare(strict_types=1);

namespace CampaignBundle\Controller;

use CampaignBundle\Entity\CampaignType;
use CampaignBundle\Form\CreateCampaignTypeType;
use CoreBundle\Controller\ControllerTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/campaign-type")
 *
 * Class CampaignTypeController
 */
final class CampaignTypeController extends FOSRestController
{
    use ControllerTrait;
    /**
     * @param Request              $request
     * @param FormFactoryInterface $factory
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @Route("", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @Model(type=CreateCampaignTypeType::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Create Campaign Type",
     *     @SWG\Schema(
     *          type="string",
     *          example={"ok"}
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request"
     * )
     */
    public function createAction(Request $request, FormFactoryInterface $factory)
    {
        $form = $factory->create(CreateCampaignTypeType::class, new CampaignType());
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return $this->handleView($this->view(['Bad Request'], 400));
        }
        if (!$form->isValid()) {
            return $this->handleView($this->view($this->invalidFormRequest($form), 400));
        }

        $this->flush($form->getData());

        return $this->handleView($this->view(['ok'], 200));
    }

    /**
     * @param $object
     */
    private function flush(CampaignType $object)
    {
        $this->getDoctrine()->getManager()->persist($object);
        $this->getDoctrine()->getManager()->flush();
    }
}
