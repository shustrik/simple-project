<?php
declare(strict_types=1);


namespace CampaignBundle\Controller;

use CampaignBundle\Entity\Campaign;
use CampaignBundle\Form\CreateCampaignType;
use CampaignBundle\Form\EditCampaignType;
use CoreBundle\Controller\ControllerTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\FormFactoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/campaign")
 *
 * Class CampaignTypeController
 */
final class CampaignController extends FOSRestController
{
    use ControllerTrait;

    /**
     * @Route("", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @Model(type=CreateCampaignType::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Create Campaign",
     *     @SWG\Schema(
     *          type="string",
     *          example={"ok"}
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request"
     * )
     *
     * @param Request                $request
     * @param FormFactoryInterface   $factory
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, FormFactoryInterface $factory)
    {
        $form = $factory->create(CreateCampaignType::class, new Campaign($this->getUser()));

        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            return $this->handleView($this->view(['Bad Request'], 400));
        }
        if (!$form->isValid()) {
            return $this->handleView($this->view($this->invalidFormRequest($form), 400));
        }
        $this->flush($form->getData());

        return $this->handleView($this->view(['ok'], 200));
    }

    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Show Campaign",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Property(
     *              property="name",
     *              type="string",
     *              example="campaign name"
     *          ),
     *          @SWG\Property(
     *              property="user",
     *              type="array",
     *              @SWG\Items(
     *                  @SWG\Property(
     *                      property="username",
     *                      type="string",
     *                      example="username"
     *                  ),
     *                  @SWG\Property(
     *                      property="email",
     *                      type="string",
     *                      example="email"
     *                  )
     *              )
     *          ),
     *          @SWG\Property(
     *              property="campaignType",
     *              type="array",
     *              @SWG\Items(
     *                  @SWG\Property(
     *                      property="name",
     *                      type="string",
     *                      example="campaignType name"
     *                  ),
     *                  @SWG\Property(
     *                      property="status",
     *                      type="string",
     *                      example="campaignType status"
     *               )
     *              )
     *          ),
     *          @SWG\Property(
     *              property="settings",
     *              type="string",
     *              example="settings"
     *          ),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request"
     * )
     *
     * @param Campaign   $campaign
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function showAction(Campaign $campaign)
    {
        return $this->handleView(
            $this->view($campaign, 200)
        );
    }



    /**
     * @Route("/{id}", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @Model(type=EditCampaignType::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Edit Campaign",
     *     @SWG\Schema(
     *          type="string",
     *          example={"ok"}
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request"
     * )
     *
     * @param Request              $request
     * @param Campaign             $campaign
     *
     * @param FormFactoryInterface $factory
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function editAction(Request $request, Campaign $campaign, FormFactoryInterface $factory)
    {
        $form = $factory->create(EditCampaignType::class, $campaign);
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return $this->handleView($this->view(['Bad Request'], 400));
        }
        if (!$form->isValid()) {
            return $this->handleView($this->view($this->invalidFormRequest($form), 400));
        }

        $this->flush($form->getData());

        return $this->handleView($this->view(['ok'], 200));
    }

    /**
     * @Route("/{id}", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Campaign Deleted",
     *     @SWG\Schema(
     *          type="string",
     *          example={"ok"}
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request"
     * )
     *
     * @param Campaign             $campaign
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function deleteAction(Campaign $campaign)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($campaign);
        $manager->flush();

        return $this->handleView($this->view(['ok'], 200));
    }

    /**
     * @param $object
     */
    private function flush(Campaign $object)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($object);
        $manager->flush();
    }
}
