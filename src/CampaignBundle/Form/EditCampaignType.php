<?php
declare(strict_types=1);

namespace CampaignBundle\Form;

use CampaignBundle\Entity\Campaign;
use CampaignBundle\Entity\CampaignType;
use CoreBundle\Form\AbstractForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class EditCampaignType extends AbstractForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('settings', TextType::class)
            ->add(
                'campaignType',
                EntityType::class,
                [
                    'class'        => CampaignType::class,
                    'choice_label' => 'name',
                    'choice_value' => 'id'
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Campaign::class,
                'method' => 'PUT'
            ]
        );
    }
}
